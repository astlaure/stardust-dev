<?php
/*
Plugin Name: Stardust Dev
Description: Dev Plugin for the Stardust Ecosystem. Provide Mailhog mailcatcher.
Version: 1.0.0
Author Name: Alexandre St-Laurent
Author URI: https://github.com/astlaure
*/

if ( !defined( 'ABSPATH' ) ) {
    exit;
}

if ( !class_exists( 'StardustDev' ) ) {
    class StardustDev {
        function __construct() {
            require_once __DIR__ . '/classes/mailhog.php';
            new StardustDevMailhog();
        }

        static public function activate() {
            require_once( ABSPATH . '/wp-admin/includes/plugin.php' );

            if ( !is_plugin_active( 'stardust-core/stardust-core.php' ) ) {
                ?>
                <div class="notice notice-error" >
                    <p><?php _e('Please enable Stardust Core before activating Stardust I18n', 'stardust-dev'); ?></p>
                </div>
                <?php
                @trigger_error(__( 'Please enable Stardust Core before activating Stardust I18n', 'stardust-dev' ), E_USER_ERROR);
            }
            
            update_option( 'rewrite_rules', '' );
        }

        static public function deactivate() {
            flush_rewrite_rules();
        }
        
        static public function uninstall() {}
    }
}

if ( class_exists( 'StardustDev' ) ) {
    register_activation_hook( __FILE__, array( 'StardustDev', 'activate' ) );
	register_deactivation_hook( __FILE__, array( 'StardustDev', 'deactivate' ) );
	register_uninstall_hook( __FILE__, array( 'StardustDev', 'uninstall' ) );

    new StardustDev();
}
