<?php

if ( !class_exists( 'StardustDevMailhog' ) ) {
    class StardustDevMailhog {
        function __construct() {
            add_action( 'phpmailer_init', array( $this, 'phpmailer_init' ) );
        }

        public function phpmailer_init( $phpmailer ) {
            $phpmailer->Host     = '127.0.0.1';
            $phpmailer->Port     = 1025;
            $phpmailer->SMTPAuth = false;
            $phpmailer->isSMTP();
        }
    }
}
